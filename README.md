# Simple sudoku solver utilizing backtracing

- <https://en.wikipedia.org/wiki/Backtracking>
- <https://en.wikipedia.org/wiki/Sudoku_solving_algorithms>

## Compilation
```console
$ ./build.sh
```

## Running
```console
$ ./solver simple.sudoku
```

## .sudoku file format
Ignores every character that is non-numeric.

Because in sudoku there are only single digit numbers you don't even need whitespaces (newlines too).

The [provided example](./simple.sudoku) is formatted only for the convenience of the reader.
