#!/bin/sh

set -xe

CC=gcc
CFLAGS="-Wall -Wextra -Wpedantic -Os"

${CC} ${CFLAGS} -o solver solver.c
