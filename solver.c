#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//#define LOG

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

#define SUDOKU_SIDE (9)

typedef enum {
	UNSET = 0,
	ONE, TWO, THREE,
	FOUR, FIVE, SIX,
	SEVEN, EIGHT, NINE,
} Cell_Value;
#define VALUE_AS_CHAR(x) (x == UNSET ? '.' : x + '0')

typedef Cell_Value Grid[SUDOKU_SIDE][SUDOKU_SIDE];

typedef struct {
	Grid g;
	bool unsolvable;
} Sudoku;

void rand_sudoku(Grid *g) {
	for (size_t y = 0; y < SUDOKU_SIDE; ++y) {
		for (size_t x = 0; x < SUDOKU_SIDE; ++x) {
			(*g)[y][x] = (Cell_Value)(rand() % 10);
		}
	}
}

bool is_cell_valid_with_value(Grid g, size_t yv, size_t xv, Cell_Value v) {
	// Row check
	for (size_t x = 0; x < SUDOKU_SIDE; ++x) {
		if (x != xv && g[yv][x] == v) {
#ifdef LOG
			printf("LOG: same value found in row at (%zu, %zu)\n", x+1, yv+1);
#endif
			return false;
		}
	}

	// Column check
	for (size_t y = 0; y < SUDOKU_SIDE; ++y) {
		if (y != yv && g[y][xv] == v) {
#ifdef LOG
			printf("LOG: same value found in column at (%zu, %zu)\n", xv+1, y+1);
#endif
			return false;
		}
	}

	// 3x3 cell check
	size_t offset_y = 0;
	if                 (yv < 3) {} // offset_y is already 0, do nothing
	else if (3 <= yv && yv < 6) offset_y = 3;
	else if (6 <= yv && yv < 9) offset_y = 6;
	else assert(0 && "unreachable");

	size_t offset_x = 0;
	if                 (xv < 3) {} // offset_x is already 0, do nothing
	else if (3 <= xv && xv < 6) offset_x = 3;
	else if (6 <= xv && xv < 9) offset_x = 6;
	else assert(0 && "unreachable");

	for (size_t y = offset_y; y < offset_y+3; ++y) {
		for (size_t x = offset_x; x < offset_x+3; ++x) {
			if (y != yv && x != xv && g[y][x] == v) {
#ifdef LOG
				printf("LOG: same value found in 3x3 cell at (%zu, %zu)\n", x+1, y+1);
#endif
				return false;
			}
		}
	}

	return true;
}

void print_sudoku(Grid s) {
		for (size_t y = 0; y < SUDOKU_SIDE; ++y) {
			for (size_t x = 0; x < SUDOKU_SIDE; ++x) {
				printf("%c ", VALUE_AS_CHAR(s[y][x]));

				if (x != SUDOKU_SIDE-1 && ((x+1) % 3 == 0)) printf("| ");
			}
			printf("\n");

			if (y != SUDOKU_SIDE-1 && ((y+1) % 3 == 0)) printf("------+-------+------\n");
		}

	printf("\e[11A\e[20D");
}

Sudoku solve_sudoku(Sudoku s) {
	print_sudoku(s.g);
	for (size_t y = 0; y < SUDOKU_SIDE; ++y) {
		for (size_t x = 0; x < SUDOKU_SIDE; ++x) {
			if (s.g[y][x] != UNSET) continue;

			for (Cell_Value v = ONE; v <= NINE; ++v) {
				if (is_cell_valid_with_value(s.g, y, x, v)) {
					s.g[y][x] = v;
					Sudoku t = solve_sudoku(s);
					if (t.unsolvable) {
						s.g[y][x] = UNSET;
						continue;
					}
					return t;
				}
			}
			if (s.g[y][x] == UNSET) {
				s.unsolvable = true;
				return s;
			}
		}
	}
	return s;
}

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

void enable_colors(void) {
	// enables colors on Windows.
#ifdef _WIN32
	HANDLE winConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD winConsoleMode = 0;
	GetConsoleMode(winConsole, &winConsoleMode);
	SetConsoleMode(winConsole, winConsoleMode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
#endif
}

bool parse_sudoku_from_file(const char *file_path, Sudoku *s) {
	assert(s);

	FILE *f = fopen(file_path, "rb");
	if (!f) {
		fprintf(stderr, "ERROR: Couldn't open file %s for reading: %s\n", file_path, strerror(errno));
		return false;
	}
	
	char buf[1024] = {0};

	size_t n = fread(buf, 1, sizeof(buf) - 1, f);
	if (n == sizeof(buf) - 1) {
		fprintf(stderr, "WARNING: Couldn't read entire file %s, read the first %zu bytes\n", file_path, n);
	}

	if (ferror(f)) {
		fprintf(stderr, "ERROR: %s\n", strerror(errno));
		goto free_and_return;
	}

	fclose(f);

	size_t number_counter = 0; // used to count how many numbers have we scanned yet
	for (size_t i = 0; buf[i] != '\0'; ++i) {
		char c = buf[i];
		if (c < '0' || c > '9') continue;

		if (number_counter >= SUDOKU_SIDE*SUDOKU_SIDE) {
			fprintf(stderr, "WARNING: Read %zu numbers but more are still present in the file, ignoring the remaining\n", number_counter);
			break;
		}

		*((Cell_Value *)(s->g) + number_counter++) = c - '0';
	}

	if (number_counter < SUDOKU_SIDE*SUDOKU_SIDE) {
		fprintf(stderr, "ERROR: Didn't read enough numbers, wanted %u but only read %zu\n", SUDOKU_SIDE*SUDOKU_SIDE, number_counter);
		return false;
	}

	return true;

free_and_return:
	if (f) fclose(f);
	return false;
}

int main(int argc, char **argv) {
	const char *program = argv[0];
	enable_colors();

	if (argc < 2) {
		fprintf(stderr, "ERROR: Didn't provide the sudoku file\n");
		fprintf(stderr, "USAGE: %s <file.sudoku>\n", program);
		return 1;
	}
	const char *sudoku_file_path = argv[1];

	Sudoku s = {0};
	if (!parse_sudoku_from_file(sudoku_file_path, &s)) return 1;

	s = solve_sudoku(s);

	print_sudoku(s.g);
	printf("\e[12B");
	printf(s.unsolvable ? "UNSOLVABLE!\n" : "SOLVED!\n");

	return 0;
}
